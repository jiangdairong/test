//
//  PhotosViewController.h
//  FOOTBALL
//
//  Created by dairong on 2014/6/24.
//  Copyright (c) 2014年 dairong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UITableView *mytableview;
@end
