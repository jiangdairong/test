//
//  PhotosViewController.m
//  FOOTBALL
//
//  Created by dairong on 2014/6/24.
//  Copyright (c) 2014年 dairong. All rights reserved.
//

#import "PhotosViewController.h"
#import "PhotosCollectionViewCell.h"
#import "PhotosCollectionView.h"
#import "myCell.h"
#import "TabBarViewController.h"


@interface PhotosViewController ()

@end

@implementation PhotosViewController{
    NSArray *array;
    
}
@synthesize mytableview;
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.mytableview.delegate=self;
    self.mytableview.dataSource=self;    // Do any additional setup after loading the view.
     array = [[NSArray alloc]initWithObjects:@"巴西.png",@"克羅埃西亞.png",@"墨西哥.png", nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
        return 4;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

        static NSString *Cellidentifier = @"Cell";
        
        myCell *Cell = [tableView dequeueReusableCellWithIdentifier:Cellidentifier];
        if(!Cell){
            Cell = [[myCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Cellidentifier];
        }
        return Cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark Collection View Methods
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [array count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellidentifier = @"Collect";
    PhotosCollectionViewCell *Collect = [collectionView dequeueReusableCellWithReuseIdentifier:cellidentifier forIndexPath:indexPath];
    
    id path = @"http://www.popdiy.com.tw/Nationsflags/AFRICA/images/%E5%96%80%E9%BA%A5%E9%9A%86.jpg";
    NSURL *url = [NSURL URLWithString:path];
    NSData *data = [NSData dataWithContentsOfURL:url];
    // UIImage *img = [[UIImage alloc] initWithData:data cache:NO];
    Collect.countryphoto.image =[ UIImage imageWithData:data];
    
//    UIImageView *imView = [[UIImageView alloc] initWithImage:img];
//    
//    [self.view addSubview:imView ];
//    
//    
//    
//    
//    Collect.countryphoto.image = [UIImage imageNamed:[array objectAtIndex:indexPath.item]];
    return Collect;
}

@end
